package Task12_1;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class DecoratedFungingWordFromString {
    public String addDecoratedFunging(String word) {
        if (word.isEmpty()) {
            return "";
        }
        return "[".concat(word.concat("]"));
    }

    public String addDecoratedFungingWordFromString(String s) {
        if (s == null) {
            return null;
        }
        return Arrays.stream(s.split(" "))
                .map(this::addDecoratedFunging)
                .collect(Collectors.joining(" "));
    }
}
