package Task12_1;


public class FlipsStringBackwards {
    public String turnOverString(String s) {
        if(s == null) {
            return null;
        }
        return new StringBuilder(s).reverse().toString();
    }

}

