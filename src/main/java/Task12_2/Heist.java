package Task12_2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Heist {
    public static void main(String[] args) {

        Vault federalVault = new Vault();

        /**
         * Узнаем какие приватные поля, методы и конструкторы есть у класса Vault
         */
        Method[] methods = federalVault.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method);
        }

        System.out.println("--------------------------------");

        Field[] fields = federalVault.getClass().getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field);
        }

        System.out.println("--------------------------------");

        Constructor<?>[] constructors = federalVault.getClass().getDeclaredConstructors();
        System.out.println(Arrays.toString(constructors));
        System.out.println("--------------------------------");

        try {
            /**
             * Получение все значений полей федерального хранилища и сохранение ее в мои переменные
             */
            Method getDollars = federalVault.getClass().getDeclaredMethod("getDollars");
            Method getEuros = federalVault.getClass().getDeclaredMethod("getEuros");
            Method getTonsOfGold = federalVault.getClass().getDeclaredMethod("getTonsOfGold");
            Method getPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("getPentagonNukesCodes");

            getDollars.setAccessible(true);
            getEuros.setAccessible(true);
            getTonsOfGold.setAccessible(true);
            getPentagonNukesCodes.setAccessible(true);


            int myDollars = (int) getDollars.invoke(federalVault);
            System.out.println(myDollars);
            int myEuros = (int) getEuros.invoke(federalVault);
            System.out.println(myEuros);
            double myTonsOfGold = (double) getTonsOfGold.invoke(federalVault);
            System.out.println(myTonsOfGold);
            String myPentagonNukesCodes = (String) getPentagonNukesCodes.invoke(federalVault);
            System.out.println(myPentagonNukesCodes);

            /**
             * Установить федеральному хранилищу для всех полей нулевые или пустые значения
              */
            Method setDollars = federalVault.getClass().getDeclaredMethod("setDollars", int.class);
            Method setEuros = federalVault.getClass().getDeclaredMethod("setEuros", int.class);
            Method setTonsOfGold = federalVault.getClass().getDeclaredMethod("setTonsOfGold", double.class);
            Method setPentagonNukesCodes = federalVault.getClass().getDeclaredMethod("setPentagonNukesCodes", String.class);

            setDollars.setAccessible(true);
            setEuros.setAccessible(true);
            setTonsOfGold.setAccessible(true);
            setPentagonNukesCodes.setAccessible(true);

            setDollars.invoke(federalVault, 0);
            setEuros.invoke(federalVault, 0);
            setTonsOfGold.invoke(federalVault, 0.0);
            setPentagonNukesCodes.invoke(federalVault, "");

            /**
             * создать собственное хранилище (с помощью приватного конструктора * класса Vault)
             * используя награбленные данные в качестве параметров
              */
            Constructor<?> constructor = federalVault.getClass().getDeclaredConstructor(int.class, int.class, double.class, String.class);
            constructor.setAccessible(true);
            Vault myVault = (Vault) constructor.newInstance(myDollars,myEuros, myTonsOfGold, myPentagonNukesCodes);

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }
}

