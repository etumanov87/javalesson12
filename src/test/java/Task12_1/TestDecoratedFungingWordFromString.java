package Task12_1;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestDecoratedFungingWordFromString {
    DecoratedFungingWordFromString decoratedFunging;

    @BeforeClass
    public void setUp() {
        decoratedFunging = new DecoratedFungingWordFromString();
    }

    @DataProvider(name = "wordDecoratedFanging")
    public Object[][] dataProviderForWordDecoratedFanging() {
        return new Object[][]{
                {"слово", "[слово]"},
                {"Множество", "[Множество]"},
                {"МАШИНА", "[МАШИНА]"},
                {"КиРпИч", "[КиРпИч]"},
                {"1231312", "[1231312]"},
                {"[test]", "[[test]]"}
        };
    }

    @Test(dataProvider = "wordDecoratedFanging")
    public void checkWordDecoratedFanging(String word, String expectedResult) {
        String actualResult = decoratedFunging.addDecoratedFunging(word);
        Assert.assertEquals(expectedResult, actualResult);
    }


    @DataProvider(name = "stringWordsDecoratedFanging")
    public Object[][] dataProviderForStringWordsDecoratedFanging() {
        return new Object[][]{
                {"слово", "[слово]"},
                {"пара слов", "[пара] [слов]"},
                {"еще немного слов", "[еще] [немного] [слов]"},
        };
    }

    @Test(dataProvider = "stringWordsDecoratedFanging")
    public void checkStringWordsDecoratedFanging(String s, String expectedResult) {
        String actualResult = decoratedFunging.addDecoratedFungingWordFromString(s);
        Assert.assertEquals(expectedResult, actualResult);
    }

    @DataProvider(name = "incorrectData")
    public Object[][] dataProviderForStringWordsDecoratedFangingIncorrectData() {
        return new Object[][]{
                {null, null},
                {"", ""},
        };
    }
    @Test(dataProvider = "incorrectData")
    public void checkStringWordsDecoratedFangingIncorrectData(String s, String expectedResult) {
        String actualResult = decoratedFunging.addDecoratedFungingWordFromString(s);
        Assert.assertEquals(expectedResult, actualResult);
    }

}
