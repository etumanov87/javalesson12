package Task12_1;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestFlipsStringBackwards {
    FlipsStringBackwards flipsStringBackwards;

    @BeforeClass
    public void setUp() {
        flipsStringBackwards = new FlipsStringBackwards();
    }

    @DataProvider(name = "wordFlipsBackwards")
    public Object[][] dataProviderForFlipsWordBackwards() {
        return new Object[][]{
                {"раз", "зар"},
                {"ДВА", "АВД"},
                {"Три", "ирТ"},
                {"я устал придумывать примеры", "ыремирп ьтавымудирп латсу я"},
                {"не знаю как придумать негативные примеры тестов", "вотсет ыремирп еынвитаген ьтамудирп как юанз ен"}
        };
    }

    @Test(dataProvider = "wordFlipsBackwards")
    public void testFlipsStringBackwards(String word, String expectedResult) {
        String actualResult = flipsStringBackwards.turnOverString(word);
        Assert.assertEquals(expectedResult, actualResult);
    }

    @DataProvider(name = "incorrectData")
    public Object[][] dataProviderForFlipsWordBackwardsIncorrectData() {
        return new Object[][]{
                {null, null},
                {"", ""},
        };
    }
    @Test(dataProvider = "incorrectData")
    public void testFlipsStringBackwardsIncorrectData(String word, String expectedResult) {
        String actualResult = flipsStringBackwards.turnOverString(word);
        Assert.assertEquals(expectedResult, actualResult);
    }
}

